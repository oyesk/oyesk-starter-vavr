#!/bin/bash

sed -i~ "/<servers>/ a\
<server>\
  <id>central</id>\
  <username>${maven_central_user}</username>\
  <password>${maven_central_password}</password>\
</server>" /usr/share/maven/conf/settings.xml

sed -i "/<profiles>/ a\
<profile>\
  <id>ossrh-oyesk</id>\
  <properties>\
      <gpg.executable>gpg</gpg.executable>\
      <gpg.keyname>oyesk-oss-maven-central</gpg.keyname>\
      <gpg.passphrase>${maven_gpg_pass_v2}</gpg.passphrase>\
  </properties>\
</profile>" /usr/share/maven/conf/settings.xml