# Oyesk Starter VAVR
This is a custom library made by OYESK team that adds the required and custom configuration for the VAVR library  through the Maven build tool.

## Features
 * Configuration that allows us to return a `Try<T>` on an endpoint, using the standard serializer to throw an exception when a try fails and to generate a json object when it succeeds. 

## Version
Current version: **2.7.8**
> **_NOTE:_** The version of this library is always the same as its parent [spring-boot-starter-parent](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-parent) version

## Usage
 1. Add this into your `pom.xml` to start using `oyesk-starter-vavr` dependency:

```xml
<!-- add oyesk-starter-vavr to dependencies -->
<dependencies>
    <dependency>
        <groupId>com.oyesk.starter</groupId>
        <artifactId>oyesk-starter-vavr</artifactId>
        <version>2.6.6</version>
    </dependency>
</dependencies>
```

 2. You can enable the features of this library by adding this line into your `application.properties`:

```properties
oyesk.vavr-config.enabled=true
```

## Custom Configuration or Override
This library can be customized to the needs of each project, if you need to change the `TryModule` that adds the `Try<T>` feature you can create a `@Bean` with the exact same name and return type and make it return
your own Module. That will override the library configuration.
This is the `Bean` provided by this library (the implementation can be consulted on the library source code): 

```java
@Bean
@ConditionalOnMissingBean
public Module getTryModule() { return new TryModule(); }
```

If only want to use this library dependencies you can just disable the configurations on your `application.properties` file, as it was shown before.

If you want to add a VAVR configuration you can create a `@Bean` with your modifications on a `@Configuration` class.

[//]: # (## Public Domain)

[//]: # (...)

[//]: # ()
[//]: # (## License)

[//]: # (...)

[//]: # ()
[//]: # ()
[//]: # (## Privacy)

[//]: # (...)

[//]: # ()
[//]: # (## Contributing)

[//]: # (...)

[//]: # ()
[//]: # (## Records)

[//]: # (...)

[//]: # ()
[//]: # (## Notices)

[//]: # (...)
