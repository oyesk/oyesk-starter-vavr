package com.oyesk.starter.oyeskstartervavr.config.vavr;

import com.fasterxml.jackson.databind.Module;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConditionalOnProperty(name = "oyesk.vavr-config.enabled", havingValue = "true")
public class VavrConfig {

    @Bean
    @ConditionalOnMissingBean
    public Module getTryModule() {
        return new TryModule();
    }
}
